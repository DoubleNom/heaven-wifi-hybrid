package io.humanitas.wifihybridmodule;

public interface WifiDevicesCallback {
    void deviceAdded(WifiDevice device);
    void deviceUpdated(WifiDevice oldDevice, WifiDevice newDevice);
    void deviceRemoved(WifiDevice device);
}