package io.humanitas.wifihybridmodule;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;

import java.util.ArrayList;
import java.util.Locale;
import java.util.regex.Pattern;

import static io.humanitas.wifihybridmodule.Helper.getBroadcastAddress;
import static io.humanitas.wifihybridmodule.Helper.parseClientIp;
import static android.net.wifi.WifiManager.NETWORK_STATE_CHANGED_ACTION;

public class WifiMain extends HandlerThread {
    public static final String TAG = "WIFI";
    
    private String heavenAddress;
    private Handler handler;
    private Context context;
    private WifiManager.MulticastLock multicastLock = null;
    private WifiManager wifiManager;
    private WifiP2pManager directManager;
    private WifiP2pManager.Channel channel;
    private UdpRx udpRx;
    private UdpTx udpTx;
    private RUdpRx rUdpRx;
    private RUdpTx rUdpTx;
    private Relay relay;
    private Devices devices;

    private String sendIpId = null;


    public WifiMain(String name, String heavenAddress) {
        super(name);
        this.heavenAddress = heavenAddress;
        this.start();
    }

    @Override
    protected void onLooperPrepared() {
        super.onLooperPrepared();
        handler = new Handler(Looper.myLooper());
    }

    @Override
    public boolean quitSafely() {
        closeWifi();
        return super.quitSafely();
    }

    @Override
    public boolean quit() {
        closeWifi();
        return super.quit();
    }

    public static void doKill(Context context) {
        WifiManager manager = (WifiManager) context
                .getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        for (WifiConfiguration configuration: manager.getConfiguredNetworks()) {
            manager.removeNetwork(configuration.networkId);
        }
        manager.setWifiEnabled(false);
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setCallback(WifiMainCallback callback) {
        this.callback = callback;
    }
    
    
//**************************************************************************************************
//  Messages
//**************************************************************************************************
    private Pattern ipPattern = Pattern.compile("^\\d{1,3}.\\d{1,3}.\\d{1,3}.\\d{1,3}.\\d{1,3}$");
    private Runnable heartbeatTask; {
        heartbeatTask = new Runnable() {
            @Override
            public void run() {
                if ( devices.getMaster() == null) {
                    Log.e(TAG, "No master to send hasHeartbeat, cancelling");
                    return;
                }
                String address = devices.getMaster().getHa();
                String message = Helper.messageHeartbeat();
                send(address, message, true);

                handler.postDelayed(this, Config.HEARTBEAT_PERIOD);
            }
        };
    }

    public String[] getNeighbours() {
        WifiDevice master = devices.getMaster();

        int length = devices.getClients().length;
        if (master != null) ++length;

        String[] neighbours = new String[length];
        length = 0;

        if(master != null) {
            neighbours[length++] = master.getHa();
        }

        for (WifiDevice device: devices.getClients()) {
            neighbours[length++] = device.getHa();
        }

        return neighbours;
    }


//**************************************************************************************************
//  Callbacks
//**************************************************************************************************
    private WifiMainCallback callback;

    private final Relay.WifiRelayCallback wifiRelayCallback = new Relay.WifiRelayCallback() {
        @Override
        public void relayCreationSuccess(int code) {
            handler.post(() -> {
                Log.d(TAG,"relayCreationSuccess: " + code);
                relayCreated();
            });
        }

        @Override
        public void relayCreationFailure(int code) {
            handler.post(() -> {
                Log.d(TAG,"relayCreationFailure: " + code);
                // TODO Notify creation failure
                if (devices.getMaster() != null) {
                    devices.getMaster().setTransferUnicast(true);
                }
            });
        }

        @Override
        public void relayStopSuccess(int code) {
            handler.post(() -> {
                Log.d(TAG,"relayStopSuccess: " + code);
                relayStopped();
            });
        }

        @Override
        public void relayStopFailure(int code) {
            Log.d(TAG,"relayStopFailure: " + code);
        }

        @Override
        public void relayConnectionFailure(int code) {
            Log.d(TAG,"relayConnectionFailure: " + code);
        }

        @Override
        public void connectionTimedOut(String heavenAddress) {
            Log.d(TAG,"connectionTimedOut: " + heavenAddress);

        }
    };
    
    private final WifiDevicesCallback wifiDevicesCallback = new WifiDevicesCallback() {
        @Override
        public void deviceAdded(WifiDevice device) {
            Log.d(TAG,"deviceAdded: " + device.toString());
            callback.peerAdded(device.getHa());
        }

        @Override
        public void deviceUpdated(WifiDevice oldDevice, WifiDevice newDevice) {
            Log.d(TAG,"deviceUpdated:\n" +
                    "New: " +
                    (System.currentTimeMillis() - newDevice.getLastHeartbeat())
                    + "\nOld: " +
                    (System.currentTimeMillis() - oldDevice.getLastHeartbeat()));
        }

        @Override
        public void deviceRemoved(WifiDevice device) {
            handler.post(() -> {
                Log.d(TAG,"deviceRemoved: " + device.toString());
                if (device.equals(devices.getMaster()))
                    Log.d(TAG,"I should have removed my master by now, what's happening?");

                // Remove device from client list if it was a client
                relay.clientDisconnected(device.getHa());

                callback.peerRemoved(device.getHa());
            });
        }
    };
    
    private final UdpRx.Callback udpCallback = new UdpRx.Callback() {
        @Override
        public void messageReceived(UdpMessage message) {
            callback.messageReceived(message.sender, message.payload);
        }
    };

    private final RUdpTx.Callback rUdpTxCallback = new RUdpTx.Callback() {
        @Override
        public void sendSuccess(RUdpMessage message) {
            callback.messageSent(message.recipient, message.payload);
        }

        @Override
        public boolean sendFailed(RUdpMessage message) {
            Log.d(TAG,"failed: " + message.toString() );
            WifiDevice device = devices.getDevice(message.recipient);
            // If device still exist, resend message
            return device != null;
        }

        @Override
        public void sendDropped(RUdpMessage message) {
            Log.d(TAG,"dropped: " + message.toString());
        }
    };

    private final RUdpRx.Callback rUdpRxCallback = new RUdpRx.Callback() {
        @Override
        public void ackReceived(RUdpMessage message) {
            if (message.id.equals(sendIpId)) {
                sendIpId = null;
                callback.wifiConnected(wifiManager.getConnectionInfo().getSSID());
                callback.peerAdded(getMasterAddress());
            }
            if(message.recipient.equals(devices.getThisHeavenAddress())) {
                rUdpTx.ackReception(message.id);
            }
        }

        @Override
        public void messageReceived(RUdpMessage message) {
            if(message.sender.equals(devices.getThisHeavenAddress())) {
                return;
            } /*else {
                Log.d(TAG,"RUDP: " + message.toString());
            }*/

            if(message.recipient.equals(devices.getThisHeavenAddress())) {
                sendAck(message);
                if(Helper.isLayerWifi(message.payload)) {
                    messageWifiReceived(message.sender, Helper.removeLayer(message.payload));
                }
                else if (Helper.isLayerHeaven(message.payload)){
                    callback.messageReceived(message.sender, Helper.removeLayer(message.payload));
                }
            }
        }

        @Override
        public void unknownMessage(UdpMessage message) {
            Log.d(TAG,"unknown: " + message.toString());
        }
    };




//**************************************************************************************************
//  Start & Stop
//**************************************************************************************************
    public void initWifi(Context context) {
        Log.d(TAG,"Starting Wi-Fi");
        this.context = context;

        devices = new Devices(heavenAddress);
        devices.setWifiDevicesCallback(wifiDevicesCallback);

        startWifi();
        if(wifiManager == null) return;

        startDirect();
        if(directManager == null | channel == null) return;

        relay = new Relay(wifiManager, directManager, channel);
        relay.registerReceiver(context);
        relay.setCallback(wifiRelayCallback);

        startSockets();
    }

    private void startWifi() {
        wifiManager = (WifiManager) context
                .getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        if(wifiManager != null) {
            if(!wifiManager.isWifiEnabled()) {
                Log.d(TAG, "Enabling wifi : " + wifiManager.setWifiEnabled(true));
                //noinspection StatementWithEmptyBody
                while (!wifiManager.isWifiEnabled());
            }
            context.registerReceiver(wifiReceiver, wifiReceiverFilter());

            multicastLock = wifiManager.createMulticastLock("HeavenMulticast");
            multicastLock.setReferenceCounted(false);
            multicastLock.acquire();
        }
    }

    private void startDirect() {
        directManager = (WifiP2pManager) context.getSystemService(Context.WIFI_P2P_SERVICE);
        if(directManager != null) {
            channel = directManager.initialize(context, Looper.getMainLooper(), null);
        }
    }

    private void startSockets() {
        udpRx = new UdpRx(Config.UDP_PORT, udpCallback);
        udpTx = new UdpTx(TAG);

        rUdpRx = new RUdpRx(Config.RUDP_PORT, rUdpRxCallback);
        rUdpTx = new RUdpTx(rUdpTxCallback);
    }

    private void stopSockets() {
        udpRx.interrupt();
        udpRx = null;
        udpTx.quit();
        udpTx = null;

        rUdpRx.interrupt();
        rUdpRx = null;
        rUdpTx.quit();
        rUdpTx = null;
    }

    private void stopDirect() {
        if (directManager != null) directManager = null;
        if (channel != null) channel = null;
    }

    public void stopWifi() {
        if(wifiManager != null) {
            try {
                context.unregisterReceiver(wifiReceiver);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (multicastLock != null) {
                if(multicastLock.isHeld()) multicastLock.release();
                multicastLock = null;
            }

            Log.d(TAG, "Stopping Wifi: " + wifiManager.setWifiEnabled(false));
            wifiManager = null;
        }
    }

    private void closeWifi() {
        Log.d(TAG,"Closing Wi-Fi");

        handler.removeCallbacksAndMessages(null);
        handler = null;

        stopSockets();

        try {
            relay.unregisterReceiver(context);
        } catch (Exception e) {
            e.printStackTrace();
        }

        stopDirect();

        relay.close();

        stopWifi();

        devices.close();
        devices = null;

        callback = null;
    }

    public void connect(String ssid, String passkey) {
        relay.connectToRelay(ssid, passkey);
    }

    public void disconnect() {
        relay.disconnect();
        callback.wifiDisconnected();
    }

    public void createRelay() {
        if(devices.getMaster() != null) {
            devices.getMaster().setTransferUnicast(false);
        }
        relay.createRelay();
    }

    public void stopRelay() {
        relay.stopRelay();
    }

    public boolean isClient() {
        return devices.getThisWifiAddress() != null;
    }

    public boolean isRelay() {
        return devices.getThisDirectAddress() != null;
    }

    public boolean isConnected() {
        return devices.getThisWifiAddress() != null || devices.getThisDirectAddress() != null;
    }

    public void removeClient(String address) {
        WifiDevice client = devices.getClient(address);
        if (client != null) {
            client.setIp(null);
            devices.setClient(client);
        }
    }

//**************************************************************************************************
//  Messages
//**************************************************************************************************
    public String send(String address, String message, boolean rUdp) {

        WifiDevice target = devices.getDevice(address);
        if(target == null) {
            Log.d(TAG,"send failed: WifiDevice not found - " + address);
            return null;
        }

        UdpMessage udpMessage = new UdpMessage();
        udpMessage.sender = devices.getThisHeavenAddress();
        udpMessage.recipient = address;
        if(target.canUnicast()) {
//            Log.d(TAG,"Unicast: " + message);
            udpMessage.isBroadcast = false;
            udpMessage.host = target.getIp();
            if (isRelay() && isClient()) {
                Log.w(TAG, "Unicast Reliable UDP from wifi relay, don't expect answers");
            }
        } else if (target.canBroadcast()) {
//            Log.d(TAG,"Broadcast: " + message);
            udpMessage.isBroadcast = true;
            udpMessage.host = getBroadcastAddress(target.getIp());
        } else {
            Log.d(TAG,"send failed: Cannot communicate with device - " +
                    target.toString());
        }

        if(!Helper.isLayerWifi(message)) message = Helper.messageHeaven(message);
        udpMessage.payload = message;

        if(rUdp) {
            udpMessage.port = Config.RUDP_PORT;
            RUdpMessage rUdpMessage = RUdpMessage
                    .createMessage(
                            devices.getThisHeavenAddress(),
                            target.getHa(),
                            udpMessage
                    );
            rUdpTx.send(rUdpMessage);
            return rUdpMessage.id;
        } else {
            udpMessage.port = Config.UDP_PORT;
            udpTx.send(udpMessage);
            return null;
        }
    }

    private void sendAck(RUdpMessage message) {
        WifiDevice target = devices.getDevice(message.sender);
        if(target == null) {
            Log.d(TAG,"Ack failed: WifiDevice not found - " + message.sender);
            return;
        }

        UdpMessage udpMessage = new UdpMessage();
        udpMessage.sender = devices.getThisHeavenAddress();
        udpMessage.recipient = message.sender;
        udpMessage.port = Config.RUDP_PORT;
        if(target.canUnicast()) {
            udpMessage.isBroadcast = false;
            udpMessage.host = target.getIp();
        } else if (target.canBroadcast()) {
            udpMessage.isBroadcast = true;
            udpMessage.host = getBroadcastAddress(target.getIp());
        } else {
            Log.d(TAG,"Ack failed: Cannot communicate with device - " +
                    target.toString());
        }

        RUdpMessage ack = RUdpMessage
                .createAck(
                        message.id,
                        message.recipient,
                        message.sender,
                        udpMessage
                );
        rUdpTx.send(ack);
    }

    private void messageWifiReceived(String sender, String message) {
        if(Helper.isHeartbeat(message)) {
            receivedHeartbeat(sender);
        }
        else if (Helper.isClientIp(message)) {
            receivedClientIp(sender, parseClientIp(message));
        }
        else {
            Log.d(TAG,"messageWifiReceived: unknown message: " + message);
        }
    }

    private void receivedHeartbeat(String sender) {
        // update devices last heartbeat in order to not consider them as disconnected
//        Log.d(TAG,"receivedHeartbeat: " + sender);
        devices.hasHeartbeat(sender);
    }

    private void receivedClientIp(String client, String ip) {
        Log.d(TAG,"receivedClientIp: ");
        if (ip.matches(ipPattern.toString())) {
            WifiDevice device = devices.getDevice(client);
            if (device == null) {
                relay.clientConnected(client);
                device = new WifiDevice(client, ip);
                devices.setClient(device);
            } else {
                device.setIp(ip);
                devices.setClient(device);
            }
        } else {
            Log.e(TAG, "messageWifiReceived: Invalid IP - " + ip);
        }
    }

    private void sendIpTask() {
        final String address = devices.getMaster().getHa();
        String message = Helper.messageClientIp(devices.getThisWifiAddress());
        sendIpId = send(address, message, true);
    }

    private void startHeartbeatTask() {
        stopHeartbeatTask();
        handler.postDelayed(heartbeatTask, Config.HEARTBEAT_PERIOD);
    }

    private void stopHeartbeatTask() {
        handler.removeCallbacks(heartbeatTask);
    }


//**************************************************************************************************
//  Relay Management
//**************************************************************************************************
    private void relayStopped() {
        // This device do not have an Direct address anymore
        devices.setThisDirectAddress(null);

        // If we have a master, we can send unicast to it once again
        if (devices.getMaster() != null) {
            devices.getMaster().setTransferUnicast(true);
        }

        // Remove clients that we had on the relay
        for(WifiDevice client: devices.getClients()) {
            client.setIp(null);
            devices.setClient(client);
        }

        callback.relayStopped();
    }

    private void relayCreated() {
        // Set this device direct address (192.168.49.1)
        devices.setThisDirectAddress(Constants.WIFI_DIRECT_RELAY_IP);

        // Notify that we cannot use unicast with master anymore if we had one
        if(devices.getMaster() != null) {
            devices.getMaster().setTransferUnicast(false);
        }
        requestGroupInfo();
    }

    private void requestGroupInfo() {
        directManager.requestGroupInfo(channel, group -> {
            if (group != null) {
                String passphrase = group.getPassphrase();
                String ssid = group.getNetworkName();
                if (passphrase != null && ssid != null) {
                    if (callback != null) callback.relayStarted(ssid, passphrase);
                    return;
                }
            }

            Log.d(TAG,"Group info not available yet, restarting");
            handler.postDelayed(this::requestGroupInfo, 1000);
        });
    }


//**************************************************************************************************
//  Broadcast receivers
//**************************************************************************************************

    private final BroadcastReceiver wifiReceiver = new BroadcastReceiver() {
        Pattern directSsidPattern = Pattern.compile("^\"DIRECT-\\w{2}-.+\"$");

        NetworkInfo.State previousState = NetworkInfo.State.UNKNOWN;

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent == null) return;
            String action = intent.getAction();

            if (action == null || !action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) return;
            NetworkInfo networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);

            if(ConnectivityManager.TYPE_WIFI != networkInfo.getType()) return;
            NetworkInfo.State wifiState = networkInfo.getState();

            if (previousState == NetworkInfo.State.UNKNOWN || previousState == wifiState){
                previousState = wifiState;
                return;
            }
            else {
                printState(wifiState);
            }

            switch (wifiState) {
                case CONNECTED:
                    // Get SSID of the network we're connected to
                    WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                    String ssid = wifiInfo.getSSID();
                    Log.d(TAG,String.format("Connected to %s", ssid));

                    // Get Wifi IP
                    int ip = wifiInfo.getIpAddress();
                    final String ipString = String.format(
                            Locale.ENGLISH,
                            "%d.%d.%d.%d",
                            (ip & 0xff),
                            (ip >> 8 & 0xff),
                            (ip >> 16 & 0xff),
                            (ip >> 24 & 0xff));
                    devices.setThisWifiAddress(ipString);

                    // TODO: check if actual relay, if not, need to leave?
                    if (directSsidPattern.matcher(ssid).matches()) {
                        Log.d(TAG,"wifiReceiver: ssid is direct relay");
                        String address = callback.getMasterAddress(ssid.replace("\"", ""));
                        if (address == null) {
                            disconnect();
                        }
                        else {
                            devices.setMaster(
                                    new WifiDevice(address, Constants.WIFI_DIRECT_RELAY_IP));
                            if(!isRelay()) {
                                devices.getMaster().setTransferUnicast(true);
                            }
                            devices.getMaster().setTransferBroadcast();
                            sendIpTask();
                            startHeartbeatTask();
                        }
                    } else {
                        Log.d(TAG,"wifiReceiver: not a relay");
                        callback.wifiConnected(ssid);
                    }
                    break;


                case DISCONNECTED:
                    // if we had a master, we are now disconnected
                    relay.disableWifiConfiguration(null);
                    if (devices.getMaster() != null && callback != null) {
                        callback.peerRemoved(devices.getMaster().getHa());
                    }
                    devices.setMaster(null);
                    devices.setThisWifiAddress(null);
                    if (callback != null) {
                        callback.wifiDisconnected();
                    }
                    break;
            }
            previousState = wifiState;

            }
        };

    private IntentFilter wifiReceiverFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(NETWORK_STATE_CHANGED_ACTION);
        return filter;
    }

    private void printState(NetworkInfo.State state) {
        if(state == NetworkInfo.State.CONNECTED) {
            Log.d(TAG,"Connected");
        } else if (state == NetworkInfo.State.CONNECTING) {
            Log.d(TAG,"Connecting");
        } else if (state == NetworkInfo.State.DISCONNECTED) {
            Log.d(TAG,"Disconnected");
        } else if (state == NetworkInfo.State.DISCONNECTING) {
            Log.d(TAG,"Disconnecting");
        } else if (state == NetworkInfo.State.SUSPENDED) {
            Log.d(TAG,"Suspended");
        } else if (state == NetworkInfo.State.UNKNOWN) {
            Log.d(TAG,"Unknown");
        }
    }


//**************************************************************************************************
//  Helpers
//**************************************************************************************************
//    public void scan() {
//        if(!scanner.scan()) {
//            Log.e(TAG, "scan: couldn't start");
//        } else {
//            Log.d(TAG,"scan: initiated");
//        }
//    }

    public int getAvailableSlots() {
        if (relay == null) return 0;
        return relay.getAvailableSlots();
    }

    public boolean hasAvailableSlots() {
        if (relay == null) return false;
        return relay.hasAvailableSlots();
    }

//    public boolean hasStableSlots() {
//        if (relay == null) return false;
//        return relay.hasStableSlots();
//    }

    public boolean isOverloaded() {
        if (relay == null) return false;
        return relay.isOverloaded();
    }

    public int getOverload() {
        if (relay == null) return 0;
        return relay.getOverload();
    }

    public void pendingClient(String sender) {
        relay.addReservedSlot(sender);
    }

    public String getMasterAddress() {
        WifiDevice master = devices.getMaster();
        if (master == null) return null;
        return master.getHa();
    }

    public boolean isOurMaster(String address) {
        if (address == null || devices.getMaster() == null) return false;
        else return address.equals(devices.getMaster().getHa());
    }

    public ArrayList<String> getClients() {
        ArrayList<String> clients = new ArrayList<>();
        for (WifiDevice device: devices.getClients()) {
            clients.add(device.getHa());
        }
        return clients;
    }

    public boolean isOurClient(String address) {
        for(WifiDevice device: devices.getClients()) {
            if (address.equals(device.getHa())) {
                return true;
            }
        }
        return false;
    }
}
