package io.humanitas.wifihybridmodule;

import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;

import static io.humanitas.wifihybridmodule.WifiMain.TAG;

public class UdpRx extends Thread {

    private final int BUFFER_MAX_SIZE = 4096;

    private final int port;
    private Callback callback;

    private DatagramSocket socket = null;
    private byte[] buffer = new byte[BUFFER_MAX_SIZE];

    UdpRx(int port, Callback callback) {
        super("UdpRx:" + port);
        this.port = port;
        this.callback = callback;
        start();
    }

    @Override
    public void interrupt() {
        super.interrupt();
        if(socket != null && !socket.isClosed()) socket.close();
    }

    @Override
    public void run() {
        super.run();
        try {
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
            socket = new DatagramSocket(null);
            socket.setReuseAddress(true);
            socket.bind(new InetSocketAddress(port));
            while(!socket.isClosed()) {
                socket.receive(packet);
                UdpMessage message = new UdpMessage(new String(buffer, 0, packet.getLength()));
                if (message.receiveIsValid()) callback.messageReceived(message);
                else Log.d(TAG, "Udp reception failure");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(socket != null && !socket.isClosed()) {
                socket.close();
            }
        }
    }

    public interface Callback {
        void messageReceived(UdpMessage message);
    }
}
