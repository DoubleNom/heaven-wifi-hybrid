package io.humanitas.wifihybridmodule;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UdpTx extends HandlerThread {
    private final String TAG;
    private Handler handler;

    UdpTx(String TAG) {
        super("UdpTx");
        this.TAG = TAG;
        this.start();
    }

    @Override
    protected void onLooperPrepared() {
        super.onLooperPrepared();
        handler = new Handler(Looper.myLooper());
    }

    public void send(final UdpMessage message) {
        if (handler == null) {
            Log.d(TAG, "send: Handler is null, abort");
            return;
        }
        if (!message.validToSend()) {
            Log.d(TAG, "UDP sending failure");
            return;
        }
        handler.post(() -> {
            DatagramSocket socket = null;
            try {
                socket = new DatagramSocket();
                socket.setReuseAddress(true);
                socket.setBroadcast(message.isBroadcast);

                InetAddress address = InetAddress.getByName(message.host);

                byte[] buffer = message.toPacket().getBytes();
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length, address, message.port);
                socket.send(packet);
            } catch (IOException e) {
                e.printStackTrace();
                Log.d(TAG, "send: exception when sending");
            } finally {
                if(socket != null && !socket.isClosed()) {
                    socket.close();
                }
            }
        });
    }
}
