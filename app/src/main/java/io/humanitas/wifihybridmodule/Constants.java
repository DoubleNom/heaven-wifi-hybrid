package io.humanitas.wifihybridmodule;

public class Constants {
    static final String WIFI_DIRECT_NETWORK = "192.168.49";
    static final String WIFI_DIRECT_RELAY_IP = "192.168.49.1";
    static final String WIFI_DIRECT_BROADCAST_IP = "192.168.49.255";
}
