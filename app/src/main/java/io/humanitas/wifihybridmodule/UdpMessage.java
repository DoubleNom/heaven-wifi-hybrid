package io.humanitas.wifihybridmodule;

import org.jetbrains.annotations.NotNull;

import java.util.Locale;

public class UdpMessage {
    // in message
    String sender;
    String recipient;
    String payload;

    // send parameters
    String host;
    int port;
    boolean isBroadcast;

    UdpMessage() {
        isBroadcast = false;
    }

    UdpMessage(String message) {
        if (message.length() < 8) {
            return;
        }

        sender = message.substring(0, 4);
        recipient = message.substring(4, 8);
        if(message.length() != 8) {
            payload = message.substring(8);
        }
    }

    boolean validToSend() {
        return !(sender == null
        || sender.length() != 4
        || recipient == null
        || recipient.length() != 4
        || payload == null
        || payload.isEmpty()
        || host == null
        || host.isEmpty()
        || port == 0
        );
    }

    boolean receiveIsValid() {
        return !(sender == null
        || sender.length() != 4
        || recipient == null
        || recipient.length() != 4
        || payload == null
        || payload.isEmpty());
    }

    String toPacket() {
        return sender + recipient + payload;
    }

    @NotNull
    @Override
    public String toString() {
        return String.format(Locale.CANADA,
                "%s -> %s:%s:%d %s - b=%s",
                sender,
                recipient, host, port,
                payload, Boolean.toString(isBroadcast));
    }
}
