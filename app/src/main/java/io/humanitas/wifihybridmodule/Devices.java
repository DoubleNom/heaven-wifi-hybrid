package io.humanitas.wifihybridmodule;

import android.os.Handler;
import android.util.Log;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import static io.humanitas.wifihybridmodule.Config.DEVICE_CHECKER;
import static io.humanitas.wifihybridmodule.WifiMain.TAG;

class Devices {

    private static final int DEVICE_ADDED = 0;
    private static final int DEVICE_UPDATED = 1;
    private static final int DEVICE_REMOVED = 2;

    // This device
    private String thisHa;  // Heaven Address
    private String thisWa;  // Wifi Address (Wi-Fi side)
    private String thisDa;  // Direct Address (Wi-Fi Direct side)

    // Peers
    private WifiDevice master;
        // Wifi Hotspot, if we manage to do : Master <- WD -> This device <- W -> Hotspot
    private ArrayList<WifiDevice> wDevices;
    private ArrayList<WifiDevice> dDevices;

    private Handler handler;


    private WifiDevicesCallback wifiDevicesCallback;

    Devices(String address) {
        setThisHeavenAddress(address);
        setThisWifiAddress(null);
        setThisDirectAddress(null);
        wDevices = new ArrayList<>();
        dDevices = new ArrayList<>();
        handler = new Handler();
    }

    void close() {
        wifiDevicesCallback = null;
        handler.removeCallbacksAndMessages(null);
        handler = null;
        dDevices = null;
        wDevices = null;
        master = null;
    }


//**************************************************************************************************
//  This WifiDevice
//**************************************************************************************************
    String getThisHeavenAddress() {
        return thisHa;
    }

    String getThisWifiAddress() {
        return thisWa;
    }

    String getThisDirectAddress() {
        return thisDa;
    }

    private void setThisHeavenAddress(String address) {
        thisHa = address;
    }
    void setThisWifiAddress(String address) {
        thisWa = address;
    }
    void setThisDirectAddress(String address) {
        thisDa = address;
    }


//**************************************************************************************************
//  Master
//**************************************************************************************************
    WifiDevice getMaster() {
        return master;
    }
    void setMaster(WifiDevice master) {
        this.master = master;
        if (master != null) {
            master.setTransferNone();
            master.setTransferBroadcast();
        }
    }


//**************************************************************************************************
//  Direct side devices
//**************************************************************************************************
    WifiDevice[] getClients() {
        //noinspection ToArrayCallWithZeroLengthArrayArgument
        return wDevices.toArray(new WifiDevice[wDevices.size()]);
    }
    WifiDevice getClient(String address) {
        return getDevice(wDevices, address);
    }
    void setClient(final WifiDevice device) {
        device.setTransferNone();
        device.setTransferUnicast(true);
        switch (setDevice(wDevices, device)) {
            case DEVICE_ADDED:
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        WifiDevice currentDevice = getClient(device.getHa());

                        if (currentDevice != null) {
                            if (currentDevice.hasHeartbeat()) {
                                Log.d(TAG, "Client with no heartbeat\n" + currentDevice.toString());
                                currentDevice.setIp(null);
                                setClient(currentDevice);
                            } else {
                                Log.d(TAG, "Client with heartbeat\n" + currentDevice.toString());
                            }
                            handler.postDelayed(this, Config.HEARTBEAT_CHECK_PERIOD);
                        }
                    }
                }, Config.HEARTBEAT_PERIOD);
                break;
            case DEVICE_REMOVED:
                if (wifiDevicesCallback != null) wifiDevicesCallback.deviceRemoved(device);
                break;
        }
    }

    private Runnable deviceRunnable = this::devicesRefresh;

    private void devicesRefresh() {
        ArrayList<WifiDevice> removeList = new ArrayList<>();
        for(WifiDevice device: wDevices) {
            if (device.hasHeartbeat()) {
                removeList.add(new WifiDevice(device));
            }
        }
        for(WifiDevice device: removeList) {
            device.setIp(null);
            setClient(device);
        }
        handler.postDelayed(deviceRunnable, DEVICE_CHECKER);
    }



////**************************************************************************************************
////  Wifi side devices
////**************************************************************************************************
//    WifiDevice[] getNeighbours() {
//        //noinspection ToArrayCallWithZeroLengthArrayArgument
//        return wDevices.toArray(new WifiDevice[wDevices.size()]);
//    }
//    WifiDevice getNeighbour(WifiDevice device) {
//        return getDevice(dDevices, device);
//    }
//    WifiDevice getNeighbour(String address) {
//        return getDevice(dDevices, address);
//    }
//    void setNeighbour(WifiDevice device){
//        device.setTransferNone();
////        TODO : Need to test if we can broadcast to neighbour
////        device.setTransferBroadcast(true);
//        setDevice(dDevices, device);
//    }


//**************************************************************************************************
//  Devices
//**************************************************************************************************
    private int findDevice(ArrayList<WifiDevice> list, WifiDevice device) {
        String ha = device.getHa();
        String ip = device.getIp();

        if(ha == null && ip == null)
            return -1;

        for(int i = 0; i < list.size(); i++) {
            WifiDevice candidate = list.get(i);
            if(ha != null && candidate.getHa() != null &&
                    ha.equals(candidate.getHa()))
                return i;
            if(ip != null && candidate.getIp() != null &&
                    ip.equals(candidate.getIp()))
                return i;
        }
        return -1;
    }
    private int findDevice(ArrayList<WifiDevice> list, String ha) {
        if(ha == null) return -1;
        for(int i = 0; i < list.size(); i++) {
            if(list.get(i).getHa() != null &&
                    ha.equals(list.get(i).getHa()))
                return i;
        }
        return -1;
    }
    WifiDevice getDevice(String address) {
        if(master != null && master.getHa().equals(address)) return master;

        int index = findDevice(wDevices, address);
        if(index != -1) return new WifiDevice(wDevices.get(index));

        index = findDevice(dDevices, address);
        if(index != -1) return new WifiDevice(dDevices.get(index));

        return null;
    }

    private WifiDevice getDevice(ArrayList<WifiDevice> list, String address) {
        int index = findDevice(list, address);
        if (index != -1) return new WifiDevice(list.get(index));
        else return null;
    }
    private int setDevice(ArrayList<WifiDevice> list, WifiDevice device) {
        int index = findDevice(list, device);
        if (index == -1) {
            list.add(device);
            if(wifiDevicesCallback != null) wifiDevicesCallback.deviceAdded(device);
            return DEVICE_ADDED;
        } else {
            if(device.getIp() == null) {
                list.remove(index);
                if(wifiDevicesCallback != null) wifiDevicesCallback.deviceRemoved(device);
                return DEVICE_REMOVED;
            } else {
                WifiDevice oldDevice = list.set(index, device);
                if(wifiDevicesCallback != null) wifiDevicesCallback.deviceUpdated(oldDevice, device);
                return DEVICE_UPDATED;
            }
        }
    }

    void hasHeartbeat(String sender) {
        WifiDevice client = getClient(sender);
        if (client == null) {
            Log.d(TAG, "Heartbeat from unknown device: " + sender);
            return;
        }
        client.doHeartbeat();
        setClient(client);
    }


//**************************************************************************************************
//  WifiDevicesCallback
//**************************************************************************************************
    void setWifiDevicesCallback(WifiDevicesCallback wifiDevicesCallback) {
        this.wifiDevicesCallback = wifiDevicesCallback;
    }


//**************************************************************************************************
//  Output
//**************************************************************************************************
    @NotNull
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("This device\n").append(
                String.format("%s : %s : %s\n", thisHa, thisWa, thisDa)
        );
        if(master != null) sb.append("Master\n").append(master.toString()).append("\n");
        else sb.append("Master\nNULL\n");

        sb.append("Wifi devices");
        if(wDevices.size() == 0) {
            sb.append("\nNULL\n");
        } else {
            for(WifiDevice device: wDevices){
                sb.append("\n").append(device.toString());
            }
            sb.append("\n");
        }

        sb.append("Direct devices");
        if(dDevices.size() == 0) {
            sb.append("\nNULL\n");
        } else {
            for(WifiDevice device: dDevices){
                sb.append("\n").append(device.toString());
            }
        }
        return sb.toString();
    }

}
