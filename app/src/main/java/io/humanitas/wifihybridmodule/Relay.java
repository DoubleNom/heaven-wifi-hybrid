package io.humanitas.wifihybridmodule;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Locale;

import static io.humanitas.wifihybridmodule.Relay.WifiRelayCallback.CREATION_BUSY;
import static io.humanitas.wifihybridmodule.Relay.WifiRelayCallback.CREATION_ERROR;
import static io.humanitas.wifihybridmodule.Relay.WifiRelayCallback.CREATION_FAILURE;
import static io.humanitas.wifihybridmodule.Relay.WifiRelayCallback.DIRECT_NOT_SUPPORTED;
import static io.humanitas.wifihybridmodule.Relay.WifiRelayCallback.NETWORK_NEW_CONFIGURATION_FAILURE;
import static io.humanitas.wifihybridmodule.Relay.WifiRelayCallback.NETWORK_UPDATE_CONFIGURATION_FAILURE;
import static io.humanitas.wifihybridmodule.Relay.WifiRelayCallback.RELAY_CREATED;
import static io.humanitas.wifihybridmodule.Relay.WifiRelayCallback.RELAY_STOPPED;
import static io.humanitas.wifihybridmodule.Relay.WifiRelayCallback.STOP_BUSY;
import static io.humanitas.wifihybridmodule.Relay.WifiRelayCallback.STOP_ERROR;
import static io.humanitas.wifihybridmodule.Relay.WifiRelayCallback.STOP_FAILURE;
import static io.humanitas.wifihybridmodule.WifiMain.TAG;
import static io.humanitas.wifihybridmodule.Config.OVERRIDE_RELAY_POWER_SAVING;

class Relay {

    private WifiManager wifiManager;
    private WifiP2pManager directManager;
    private WifiP2pManager.Channel channel;

    private Handler handler;

    private HashSet<String> clients;
    private HashSet<String> waiting;

    private WifiRelayCallback callback;


//* Relay ******************************************************************************************

    private Runnable emptyRelayDeadline = () -> {
        Log.d(TAG, "Deadline: Empty relay");
        stopRelay();
    };

    Relay (WifiManager wifiManager, WifiP2pManager directManager, WifiP2pManager.Channel channel) {
        this.wifiManager = wifiManager;
        this.directManager = directManager;
        this.channel = channel;
        handler = new Handler(Looper.myLooper());
        clients = new HashSet<>(9);
        waiting = new HashSet<>(9);
        disableAllWifiConfiguration();
        eraseAllDirectGroup();
    }

    void close() {
        callback = null;
        disableAllWifiConfiguration();
        eraseAllDirectGroup();
        stopRelay();

        handler.removeCallbacksAndMessages(null);
        handler = null;
        wifiManager = null;
        directManager = null;
        channel = null;
    }

    void createRelay() {
        if (directManager == null) {
            callback.relayCreationFailure(DIRECT_NOT_SUPPORTED);
            return;
        }

        directManager.createGroup(channel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Log.i(TAG, "Relay Created");
                clients = new HashSet<>(9);
                waiting = new HashSet<>(9);
                checkEmptyRelay();

                callback.relayCreationSuccess(RELAY_CREATED);
            }

            @Override
            public void onFailure(int reason) {
                switch (reason) {
                    case WifiP2pManager.BUSY:
                        Log.i(TAG, "Relay failure: busy");
                        callback.relayCreationFailure(CREATION_BUSY);
                        break;
                    case WifiP2pManager.ERROR:
                        Log.i(TAG, "Relay failure: error");
                        callback.relayCreationFailure(CREATION_ERROR);
                        break;
                    default:
                        Log.i(TAG, "Relay failure: unknown");
                        callback.relayCreationFailure(CREATION_FAILURE);
                }
            }
        });
    }

    void stopRelay() {
        try {
            if (directManager == null) {
                callback.relayStopFailure(DIRECT_NOT_SUPPORTED);
                return;
            }

            directManager.removeGroup(channel, new WifiP2pManager.ActionListener() {
                @Override
                public void onSuccess() {
                    try {
                        handler.removeCallbacks(emptyRelayDeadline);
                        callback.relayStopSuccess(RELAY_STOPPED);
                        clients.clear();
                        waiting.clear();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int reason) {
                    try {
                        switch (reason) {
                            case WifiP2pManager.BUSY:
                                callback.relayStopFailure(STOP_BUSY);
                                break;
                            case WifiP2pManager.ERROR:
                                callback.relayStopFailure(STOP_ERROR);
                                break;
                            default:
                                callback.relayStopFailure(STOP_FAILURE);
                                break;
                        }
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    void connectToRelay(String SSID, String passkey) {
        SSID = String.format("\"%s\"", SSID.replace("\"", ""));
        passkey = String.format("\"%s\"", passkey.replace("\"", ""));
        int netId;

        WifiConfiguration configuration = getWifiConfiguration(SSID);
        if (configuration == null) {
            Log.d(TAG,"connectToRelay: conf is empty, adding it");
            netId = addWifiConfiguration(SSID, passkey);
        } else {
            Log.d(TAG,"connectToRelay: updating conf");
            netId = updateWifiConfiguration(configuration, passkey);
        }

        if (callback != null) {
            if (netId < 0) {
                if (configuration == null)
                    callback.relayConnectionFailure(NETWORK_NEW_CONFIGURATION_FAILURE);
                else
                    callback.relayConnectionFailure(NETWORK_UPDATE_CONFIGURATION_FAILURE);
                return;
            }
        }

        wifiManager.disconnect();
        disableAllWifiConfiguration();
        wifiManager.enableNetwork(netId, true);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) wifiManager.saveConfiguration();
        wifiManager.reconnect();
        wifiManager.reassociate();
     }

    void disconnect() {
        try {
            if (wifiManager.isWifiEnabled()) {
                int netId = wifiManager.getConnectionInfo().getNetworkId();
                wifiManager.disconnect();
                wifiManager.disableNetwork(netId);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) wifiManager.saveConfiguration();
            }
        } catch (NullPointerException ignore) {}
    }

//* Clients ****************************************************************************************
    private void checkEmptyRelay() {
        if (OVERRIDE_RELAY_POWER_SAVING) return;

        Log.d(TAG,String.format(
                Locale.CANADA,
                "checkEmptyRelay: C%d/W%d/R%d/M%d",
                clients.size(),
                waiting.size(),
                Config.RELAY_RECOMMENDED_CLIENTS,
                Config.RELAY_MAX_CLIENTS
        ));

        if (clients.isEmpty() && waiting.isEmpty()) {
            Log.d(TAG,"checkEmptyRelay: no slots used");
            handler.postDelayed(emptyRelayDeadline, Config.EMPTY_RELAY_DEADLINE);
        } else {
            Log.d(TAG,"checkEmptyRelay: slots used, will continue");
            handler.removeCallbacks(emptyRelayDeadline);
        }
    }

    /**
     * Reserve a connection slot after a client connection request.
     * Client must connect before the end of the timer
     * <code>Config.RESERVED_SLOT_DEADLINE</code>.
     *
     * @param heavenAddress Address of the client
     */
    void addReservedSlot(final String heavenAddress) {
        if (clients.contains(heavenAddress)) {
            Log.d(TAG,"addReservedSlot: " + heavenAddress + " is already a client ");
            return;
        }
        if (!waiting.contains(heavenAddress)) {
            checkEmptyRelay();
        }
        waiting.add(heavenAddress);
        handler.postDelayed(() -> {
                if (waiting.contains(heavenAddress)) {
                    waiting.remove(heavenAddress);
                    checkEmptyRelay();
                    callback.connectionTimedOut(heavenAddress);
                }
            },
            Config.RESERVED_SLOT_DEADLINE
        );
    }

    void clientConnected(String heavenAddress) {
        Log.d(TAG,"clientConnected: " + heavenAddress);
        if(waiting.contains(heavenAddress)) {
            while (waiting.contains(heavenAddress))
                waiting.remove(heavenAddress);
        }
        clients.add(heavenAddress);
        checkEmptyRelay();
    }

    void clientDisconnected(String heavenAddress) {
        Log.d(TAG,"clientDisconnected: " + heavenAddress);
        clients.remove(heavenAddress);
        checkEmptyRelay();
    }


//* Wifi Direct ************************************************************************************

    int getAvailableSlots() {
        int slotsUsed = clients.size() + waiting.size();
        return Config.RELAY_MAX_CLIENTS - slotsUsed;
    }

    boolean hasAvailableSlots() {
        int slotsUsed = clients.size() + waiting.size();
        return Config.RELAY_MAX_CLIENTS > slotsUsed;
    }

    boolean isOverloaded() {
        int slotsUsed = clients.size();
        return slotsUsed > Config.RELAY_RECOMMENDED_CLIENTS;
    }

    int getOverload() {
        int slotsUsed = clients.size();
        int overload = slotsUsed - Config.RELAY_RECOMMENDED_CLIENTS;
        if (overload < 0) overload = 0;
        return overload;
    }


//* Wifi Configuration *****************************************************************************
    private WifiConfiguration getWifiConfiguration(String SSID) {
        SSID = s2conf(SSID);
        for (WifiConfiguration conf : wifiManager.getConfiguredNetworks()) {
            if (conf.SSID != null && conf.SSID.equals(SSID)) {
                return conf;
            }
        }
        return null;
    }

    private int addWifiConfiguration(String SSID, String passkey) {
        if (wifiManager == null) return -1;
        WifiConfiguration configuration = new WifiConfiguration();
        configuration.SSID = s2conf(SSID);
        configuration.preSharedKey = s2conf(passkey);
        int netId = wifiManager.addNetwork(configuration);
        wifiManager.enableNetwork(netId, false);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) wifiManager.saveConfiguration();
        return netId;
    }

    private int updateWifiConfiguration(WifiConfiguration configuration, String passkey) {
        if (wifiManager == null) return -1;
        configuration.preSharedKey = s2conf(passkey);
        int netId = wifiManager.updateNetwork(configuration);
        wifiManager.enableNetwork(netId, false);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) wifiManager.saveConfiguration();
        return netId;
    }

    void disableWifiConfiguration(String SSID) {
        if (SSID != null) SSID = s2conf(SSID);
            for (WifiConfiguration configuration: wifiManager.getConfiguredNetworks()) {
            if (SSID == null || SSID.equals(configuration.SSID)) {
                wifiManager.removeNetwork(configuration.networkId);
//                wifiManager.disableNetwork(configuration.networkId);
                break;
            }
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) wifiManager.saveConfiguration();
    }

    private void disableAllWifiConfiguration() {
        for (WifiConfiguration configuration : wifiManager.getConfiguredNetworks()) {
            wifiManager.disableNetwork(configuration.networkId);
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) wifiManager.saveConfiguration();
    }

    private void eraseAllDirectGroup() {
        try {
            Method[] methods = WifiP2pManager.class.getMethods();
            for (Method method : methods) {
                if (method.getName().equals("deletePersistentGroup")) {
                    // Delete any persistent group
                    for (int netid = 0; netid < 32; netid++) {
                        method.invoke(directManager, channel, netid, null);
                    }
                }
            }
            Log.i(TAG, "Persistent groups removed");
        } catch (NullPointerException | IllegalAccessException |InvocationTargetException e) {
            e.printStackTrace();
        }
    }


//* Callbacks **************************************************************************************
    void setCallback(WifiRelayCallback callback) {
        this.callback = callback;
    }



//* Utils ******************************************************************************************

    /**
     * String to configuration
     * Format string for configuration by making sure the string is surrounded by double quotes
     * @param arg String to format
     * @return String formatted
     */
    private String s2conf(String arg) {
        return String.format("\"%s\"", arg.replace("\"", ""));
    }

    void registerReceiver(Context context) {
        context.registerReceiver(broadcastReceiver, relayIntentFilter());
    }
    void unregisterReceiver(Context context) {
        context.unregisterReceiver(broadcastReceiver);
    }

    private IntentFilter relayIntentFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        return filter;
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action;
            if(intent == null || (action = intent.getAction()) == null) return;
            if(action.equals(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION)) {
                WifiP2pDeviceList peers =
                        intent.getParcelableExtra(WifiP2pManager.EXTRA_P2P_DEVICE_LIST);

//                directManager.requestPeers(channel, peers1 -> {
//                    if(peers1 == null) return;
//                    Log.d(TAG, peers1.toString());
//                });
//
//                directManager.requestPeers(channel, peers12 -> {
//
//                });
//
//                handler.postDelayed(() -> directManager.requestPeers(channel, peers1 -> {
//                    if(peers1 == null) return;
//                    Log.d(TAG, peers1.toString());
//                }), 10000);
//
//                if(peers == null) return;
//                Log.d(TAG, peers.toString());
            }
        }
    };


    interface WifiRelayCallback {
        int RELAY_CREATED = 1;
        int CREATION_BUSY = 11;
        int CREATION_ERROR = 12;
        int CREATION_FAILURE = 13;

        int RELAY_STOPPED = 2;
        int STOP_BUSY = 21;
        int STOP_ERROR = 22;
        int STOP_FAILURE = 23;

        int NETWORK_NEW_CONFIGURATION_FAILURE = 31;
        int NETWORK_UPDATE_CONFIGURATION_FAILURE = 32;

        int DIRECT_NOT_SUPPORTED = 41;

        void relayCreationSuccess(int code);

        void relayCreationFailure(int code);

        void relayStopSuccess(int code);

        void relayStopFailure(int code);

        void relayConnectionFailure(int code);

        void connectionTimedOut(String heavenAddress);
    }
}
