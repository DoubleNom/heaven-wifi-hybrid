package io.humanitas.wifihybridmodule;

import org.jetbrains.annotations.NotNull;

import java.util.Locale;

public class WifiDevice {

    private static final int TRANSFER_UNICAST = 0b100;
    private static final int TRANSFER_NOT_UNICAST = 0b011;
    private static final int TRANSFER_BROADCAST = 0b010;
    private static final int TRANSFER_NONE = 0b000;


    private String ha; // Heaven Address of this device
    private String ip; // Ip Address of the device
    private long lastHeartbeat;
    private int transfer;

//**************************************************************************************************
//  Constructors
//**************************************************************************************************
    private WifiDevice() {
        transfer = TRANSFER_NONE;
        lastHeartbeat = System.currentTimeMillis();
    }

    WifiDevice(String heavenAddress, String ip) {
        this();
        ha = heavenAddress;
        setIp(ip);
    }

    WifiDevice(WifiDevice wifiDevice) {
        this.ha = wifiDevice.ha;
        this.ip = wifiDevice.ip;
        this.transfer = wifiDevice.transfer;
        this.lastHeartbeat = wifiDevice.lastHeartbeat;
    }


//**************************************************************************************************
//  Heaven Address
//**************************************************************************************************
    String getHa() {
        return ha;
    }


//**************************************************************************************************
//  Ip Address
//**************************************************************************************************
    void setIp(String ip) {
        this.ip = ip;
    }

    String getIp() {
        return ip;
    }


// Heartbeat ****************************************************************************************
    void doHeartbeat() {
        lastHeartbeat = System.currentTimeMillis();
    }

    boolean hasHeartbeat() {
        return (System.currentTimeMillis() - lastHeartbeat) >= Config.HEARTBEAT_CHECK_PERIOD;
    }

    long getLastHeartbeat() {
        return lastHeartbeat;
    }


    void setTransferUnicast(boolean able) {
        if(able) transfer |= TRANSFER_UNICAST;
        else transfer &= TRANSFER_NOT_UNICAST;
    }

    void setTransferBroadcast() {
        transfer |= TRANSFER_BROADCAST;
    }

    void setTransferNone() {
        transfer = TRANSFER_NONE;
    }

    boolean canUnicast() {
        return (transfer & TRANSFER_UNICAST) != 0;
    }

    boolean canBroadcast() {
        return (transfer & TRANSFER_BROADCAST) != 0;
    }

    @NotNull
    public String toString() {
        return String.format(Locale.CANADA,"%s;%s;%s",
                ha, ip, Tools.printTimestamp(lastHeartbeat));
    }
    
}
