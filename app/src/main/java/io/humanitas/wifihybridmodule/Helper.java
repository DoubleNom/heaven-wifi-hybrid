package io.humanitas.wifihybridmodule;

import android.util.Log;

import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import static io.humanitas.wifihybridmodule.WifiMain.TAG;

public class Helper {

    // Message
    // [Layer][Tag][Payload]
    // [   2B][ 2B][     ~B]
    //        [------------] : removeLayer
    //        [---]          : getTag
    //             [-------] : getPayload


    static String getBroadcastAddress(String ip) {
        if(ip.contains(Constants.WIFI_DIRECT_NETWORK)) {
            return Constants.WIFI_DIRECT_BROADCAST_IP;
        } else {
            return getWifiBroadcastAddress(ip);
        }
    }

    private static String getWifiBroadcastAddress(String wifiIp) {
        try {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            while(interfaces.hasMoreElements()) {
                NetworkInterface networkInterface = interfaces.nextElement();
                if(networkInterface.isLoopback()) continue;
                for(InterfaceAddress address: networkInterface.getInterfaceAddresses()) {
                    InetAddress broadcast = address.getBroadcast();
                    if(address.toString().replace("\\", "").equals(wifiIp)) {
                        Log.i(TAG, "getWifiBroadcastAddress: " + address.toString());
                    }
                    if(broadcast == null) continue;
                    return broadcast.toString().replace("/", "");
                }
            }
            return null;
        } catch (SocketException e) {
            return null;
        }
    }


//* LAYER ******************************************************************************************

    public static String removeLayer(String message) {
        return message.substring(2);
    }


//* LAYER - HEAVEN *********************************************************************************

    private static final String LAYER_HEAVEN = "LH";

    public static boolean isLayerHeaven(String message) {
        return message
                .substring(0, LAYER_HEAVEN.length())
                .equals(LAYER_HEAVEN);
    }

    static String messageHeaven(String message) {
        return LAYER_HEAVEN + message;
    }

//* LAYER - WIFI ***********************************************************************************

    private static final String LAYER_WIFI = "LW";

    static boolean isLayerWifi(String message) {
        return message
                .substring(0, LAYER_WIFI.length())
                .equals(LAYER_WIFI);
    }


    //* Heartbeat *//
    private static final String HEARTBEAT = "HB";

    static boolean isHeartbeat(String message) {
        return message.equals(HEARTBEAT);
    }

    static String messageHeartbeat() {
        return LAYER_WIFI + HEARTBEAT;
    }


    //* Client IP *//
    private static final String CLIENT_IP = "CI";

    static boolean isClientIp(String message) {
        return message
                .substring(0, CLIENT_IP.length())
                .equals(CLIENT_IP);
    }

    static String messageClientIp(String ipValue) {
        return LAYER_WIFI + CLIENT_IP + ipValue;
    }

    static String parseClientIp(String message) {
        return message.substring(CLIENT_IP.length());
    }


}
