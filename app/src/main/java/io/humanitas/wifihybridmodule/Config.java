package io.humanitas.wifihybridmodule;

class Config {

//**************************************************************************************************
//  SOCKETS
//**************************************************************************************************
    static final int UDP_PORT = 40000;
    static final int RUDP_PORT = 40001;


//**************************************************************************************************
//  Relay
//**************************************************************************************************
    static final int RELAY_MAX_CLIENTS = 2;
    static final int RELAY_RECOMMENDED_CLIENTS = 1;
    static final long EMPTY_RELAY_DEADLINE = 2  * 60 * 1000L; // 2 minutes
    static final long RESERVED_SLOT_DEADLINE = 2 * 60 * 1000L; // 2  minutes
    static final long HEARTBEAT_PERIOD = 5 * 1000L; // 15 seconds
    static final long HEARTBEAT_CHECK_PERIOD = HEARTBEAT_PERIOD * 3;
    static final long DEVICE_CHECKER = 4 * 1000L; // 4 seconds

    static boolean OVERRIDE_RELAY_POWER_SAVING = false;
}

