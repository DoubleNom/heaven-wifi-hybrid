package io.humanitas.wifihybridmodule;

import org.jetbrains.annotations.NotNull;

public class RUdpMessage {
    private static final int ID_SIZE = 3;
    private static final String ACK = Character.toString((char) 6);
    // ASCII 6 = ACK

    public String id;
    public String sender;
    String recipient;
    public String payload;

    private String host;
    private int port;
    private boolean isBroadcast;
    private int maxFailure;
    private int sendingFailure;

    private RUdpMessage() {
        id = null;
        sender = null;
        recipient = null;
        payload = null;

        host = null;
        port = 0;
        isBroadcast = false;
        maxFailure = 5;
        sendingFailure = 0;
    }

    RUdpMessage(UdpMessage packet) {
        this();
        try {
            sender = packet.sender;
            recipient = packet.recipient;
            id = packet.payload.substring(0, ID_SIZE);
            payload = packet.payload.substring(ID_SIZE);
        } catch(ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            id = null;
        }
    }

    boolean failed() {
        sendingFailure++;
        return (sendingFailure > maxFailure);
    }

    boolean isAck() {
        // No important field must be empty in order to be a message
        if(id == null || sender == null || recipient == null || payload == null) {
            return false;
        }
        // Check if payload is ACK
        return ACK.equals(payload);
    }

    public boolean isMessage() {
        // No important field must be empty in order to be a message
        if(id == null || sender == null || recipient == null || payload == null) {
            return false;
        }
        // Must no be an ACK in order to be a message
        return !ACK.equals(payload);
    }

    private static RUdpMessage createAck(
            String id,
            String sender, String recipient,
            String host, int port, boolean isBroadcast
    ) {
        RUdpMessage msg = new RUdpMessage();
        msg.id = id;
        msg.sender = sender;
        msg.recipient = recipient;
        msg.payload = ACK;
        msg.host = host;
        msg.port = port;
        msg.isBroadcast = isBroadcast;
        return msg;
    }

    static RUdpMessage createAck(
            String id,
            String sender, String recipient,
            UdpMessage udpMessage
    ) {
        return createAck(
                id,
                sender, recipient,
                udpMessage.host, udpMessage.port, udpMessage.isBroadcast
        );
    }

    private static RUdpMessage createMessage(
            String sender, String recipient, String payload,
            String host, int port, boolean isBroadcast
    ) {
        RUdpMessage msg = new RUdpMessage();
        msg.id = Tools.getRandomString(ID_SIZE);
        msg.sender = sender;
        msg.recipient = recipient;
        msg.host = host;
        msg.port = port;
        msg.payload = payload;
        msg.isBroadcast = isBroadcast;
        return msg;
    }

    static RUdpMessage createMessage(
            String sender, String recipient,
            UdpMessage udpMessage) {
        return createMessage(sender, recipient, udpMessage.payload,
                udpMessage.host, udpMessage.port, udpMessage.isBroadcast);
    }

    UdpMessage toUdpMessage() {
        UdpMessage message = new UdpMessage();
        message.sender = this.sender;
        message.recipient = this.recipient;
        message.payload = id + payload;
        message.host = this.host;
        message.port = this.port;
        message.isBroadcast = this.isBroadcast;
        return message;
    }

    @NotNull
    public String toString() {
        if (host != null) {
            return String.format("%s %s->%s:%s:%s:%s %s",
                    id,
                    sender,
                    recipient,
                    host,
                    port,
                    Boolean.toString(isBroadcast),
                    payload);
        } else {
            return String.format("%s %s->%s %s",
                    id,
                    sender,
                    recipient,
                    payload);
        }
    }
}
