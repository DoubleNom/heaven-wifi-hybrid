package io.humanitas.wifihybridmodule;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.*;

public class Tools {
//    private final static String UUID_BASE = "00000000-0000-1000-8000-00805f9b34fb";
    private static final String SHARED_PREF_HEAVEN_CONFIG = "Heaven Config";
    private static final String PREF_HEAVEN_ADDRESS = "Heaven Address";
    private static final String PREF_FORBIDDEN_NEIGHBOURS = "Forbidden neighbours";

    //* Heaven Address

    private static String formatHeavenAddress(String address) {
        if (address == null) return null;

        address = address.trim();
        address = address.toLowerCase();
        return address;
    }

    private static boolean isHeavenAddressValid(String address) {
        if (address == null || address.length() != 4)
            return false;
        for (char character: address.toCharArray()) {
            if (character < 'a' || character > 'z') return false;
        }
        return true;
    }

    private static String uuidToHeavenAddress(String uuid) {
        char[] c = new char[4];
        for(int i = 0; i < 4; i++) {
            BigInteger value = new BigInteger(uuid.substring(i * 8, i * 8 + 8), 16);
            value = value.mod(BigInteger.valueOf(26));
            c[i] = (char) (value.intValue() + 'a');
        }

        return String.format("%c%c%c%c", c[0], c[1], c[2], c[3]);
    }


    //** this device Heaven Address

    @SuppressLint("ApplySharedPref")
    static boolean setHeavenAddress(Context context, String address) {

        address = formatHeavenAddress(address);
        if (isHeavenAddressValid(address)) {
            SharedPreferences sharedPreferences =
                    context.getSharedPreferences(SHARED_PREF_HEAVEN_CONFIG, Context.MODE_PRIVATE);

            sharedPreferences.edit().putString(PREF_HEAVEN_ADDRESS, address).commit();
            return true;
        } else {
            return false;
        }
    }

    //* Timestamp
    public static String printTimestamp(long timestamp) {

        String result = "";
        String format;

        format = "HH:mm:ss";


        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.CANADA);
        try { result = sdf.format(timestamp);
        } catch (IllegalArgumentException ignore) {}

        return result;
    }


    //* Random string

    public static String getRandomString(int length) {
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0; i < length; i++) {
            stringBuilder.append((char)(new Random().nextInt(26) + 'a'));
        }
        return stringBuilder.toString();
    }

//    public static String getRandomSizedString(int length, int min) {
//        length = new Random().nextInt(length - min) + min;
//        StringBuilder stringBuilder = new StringBuilder();
//        for(int i = 0; i < length; i++) {
//            char c = (char)(new Random().nextInt(27) + 'a');
//            if(c == '{') c = ' ';
//            stringBuilder.append(c);
//        }
//        return stringBuilder.toString();
//    }

    public static boolean equals(String s1, String s2) {
        return s1 == null ? s2 == null : s1.equals(s2);
    }
}
