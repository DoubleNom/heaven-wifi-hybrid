package io.humanitas.wifihybridmodule;

public class RUdpRx {
    private UdpRx udpRx;

    public interface Callback {
        void ackReceived(RUdpMessage message);
        void messageReceived(RUdpMessage message);
        void unknownMessage(UdpMessage message);
    }

    RUdpRx(int port, final Callback callback) {
        udpRx = new UdpRx(port, message -> {
            RUdpMessage rUdpMessage = new RUdpMessage(message);
            if (rUdpMessage.isAck()) {
                callback.ackReceived(rUdpMessage);
            } else if (rUdpMessage.isMessage()) {
                callback.messageReceived(rUdpMessage);
            } else {
                callback.unknownMessage(message);
            }
        });
    }

    void interrupt() {
        if(udpRx != null && udpRx.isAlive())
            udpRx.interrupt();
    }

}
