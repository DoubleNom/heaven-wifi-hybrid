package io.humanitas.wifihybridmodule;

public interface WifiMainCallback {
    void relayStarted(String ssid, String passkey);
    void relayStopped();
    void wifiConnected(String ssid, String masterAddress);
    void wifiDisconnected();

    void peerAdded(String ha);
    void peerRemoved(String ha);

    void messageSent(String target, String payload);
    void messageReceived(String sender, String payload);
}
