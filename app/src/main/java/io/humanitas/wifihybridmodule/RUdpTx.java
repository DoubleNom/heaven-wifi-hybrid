package io.humanitas.wifihybridmodule;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;

import java.util.ArrayList;
import java.util.concurrent.LinkedTransferQueue;

import static io.humanitas.wifihybridmodule.WifiMain.TAG;

public class RUdpTx extends HandlerThread {
    private static final int ACK_TIMEOUT = 2500;

    private LinkedTransferQueue<RUdpMessage> ackQueue;
    private LinkedTransferQueue<RUdpMessage> resendQueue;
    private LinkedTransferQueue<RUdpMessage> sendQueue;
    private ArrayList<RUdpMessage> sendingList;

    private UdpTx udpTx;
    private Handler handler;


    RUdpTx(Callback callback) {
        super("UdpTx");
        ackQueue = new LinkedTransferQueue<>();
        resendQueue = new LinkedTransferQueue<>();
        sendQueue = new LinkedTransferQueue<>();
        sendingList = new ArrayList<>();
        this.callback = callback;
        this.start();
        udpTx = new UdpTx(TAG);
    }

    @Override
    protected void onLooperPrepared() {
        super.onLooperPrepared();
        handler = new Handler(Looper.myLooper());
    }

    @Override
    public boolean quit() {
        udpTx.quit();
        return super.quit();
    }

    @Override
    public boolean quitSafely() {
        udpTx.quitSafely();
        return super.quitSafely();
    }

    void ackReception(String id) {
        RUdpMessage toRemove = null;
        for(RUdpMessage message: sendingList){
            if(id.equals(message.id)) {
                toRemove = message;
                break;
            }
        }

        if(toRemove != null) {
            callback.sendSuccess(toRemove);
            sendingList.remove(toRemove);
        }
    }

    public void send(RUdpMessage message) {
        if (message.isAck()) {
            ackQueue.add(message);
            post();
        } else if (message.isMessage()) {
            sendQueue.add(message);
            post();
        } else {
            Log.d(TAG, "send: fail, not ACK nor MSG");
        }
    }
    private void post() {
        if(handler != null)
            handler.post(send);
    }

    private Runnable send = new Runnable() {
        @Override
        public void run() {
            final RUdpMessage msg = getMessage();
            if(msg == null)  {
                Log.d(TAG, "send: msg is null, abort");
                return;
            }

            udpTx.send(msg.toUdpMessage());
            if(msg.isAck()) {
//                Log.d(TAG, "send: msg is ack, task finished");
                return;
            }

            sendingList.add(msg);

            handler.postDelayed(() -> {
                if(sendingList.contains(msg)) {
                    sendingList.remove(msg);
//                        Log.d(TAG, "send: " + Arrays.toString(sendingList.toArray()) +
//                                " contains " + msg.toString());
                    if(msg.failed()) {
                        Log.d(TAG, "send: msg dropped");
                        callback.sendDropped(msg);
                    } else {
                        Log.d(TAG, "send: msg failed");
                        boolean resend = callback.sendFailed(msg);
                        if (resend) {
                            resendQueue.add(msg);
                            post();
                        } else {
                            callback.sendDropped(msg);
                        }
                    }
                }
            }, ACK_TIMEOUT);
        }
    };

    private RUdpMessage getMessage() {
        RUdpMessage msg;
        msg = ackQueue.poll();
        if (msg != null) return msg;
        msg = resendQueue.poll();
        if (msg != null) return msg;
        msg = sendQueue.poll();
        return msg;
    }

    public interface Callback {
        void sendSuccess(RUdpMessage message);
        boolean sendFailed(RUdpMessage message);
        void sendDropped(RUdpMessage message);
    }
    private Callback callback;


}
